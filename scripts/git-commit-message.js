#!/usr/bin/env node
'use strict';
const fs = require('fs');
const path = require('path');
/**
 * Default location of the git commit message.
 */
const COMMIT_MESSAGE_LOCATION = path.join('.git', 'COMMIT_EDITMSG');
const BRANCH_NAME_LOCATION = path.join('.git', 'HEAD');
const commitMessage = fs.readFileSync(COMMIT_MESSAGE_LOCATION, 'utf8');
const branch = fs.readFileSync(BRANCH_NAME_LOCATION, 'utf8');

const jiraTicketStartRegex = /^[A-Z]+-[0-9]+/;

const commitMessageStartsWithJiraTicket = (message) => {
  return !!message.match(jiraTicketStartRegex);
};


if (!commitMessageStartsWithJiraTicket(commitMessage)) {

  /**
   * For example: PROJECT-143
   */
  const jiraTicketRegex = /[A-Z]+-[0-9]+/;
  /**
   * From a given branch name extract the Jira ticket number.
   * @param branchName the branch name
   * @returns the Jira ticket number.
   */
  const getJiraTicket = branchName => jiraTicketRegex.exec(branchName)[0];
  /**
   * Checks and updates the commit message with the given jira ticket number
   * @param jiraTicket the jira ticket number
   */
  const checkAndUpdateCommitMessage = (jiraTicket) => {
    if (commitMessage.length < jiraTicket.length
        || commitMessage.substring(0, jiraTicket.length) !== jiraTicket) {
      fs.writeFileSync(COMMIT_MESSAGE_LOCATION, `${jiraTicket}: ${commitMessage}`, 'utf8');
    }
  };
  const jiraTicket = getJiraTicket(branch);
  checkAndUpdateCommitMessage(jiraTicket);
}
